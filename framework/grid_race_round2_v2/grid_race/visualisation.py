import argparse
import pygame
from judge import replay

from typing import Optional

class Screen:
    MARGIN = 10
    DEFAULT_TRACK_CELL_SIZE = 20
    TRACK_COLOURS = {
        -1: (255, 0, 0),
        0: (255, 255, 255),
        1: (255, 255, 255),
        100: (0, 0, 255),
    }
    TRACK_GRID_COLOUR = (50, 50, 50, 100)
    # stolen from Matplotlib's tab10
    PLAYER_COLOURS = [[31, 119, 180], [255, 127, 14], [44, 160, 44],
                      [214, 39, 40], [148, 103, 189], [140, 86, 75],
                      [227, 119, 194], [127, 127, 127], [188, 189, 34],
                      [23, 190, 207]]
    FONT_SIZE = 30
    FONT_COLOUR = (255, 255, 255)

    def __init__(self, env_info: replay.EnvInfo, cell_size: int):
        track_width = len(env_info.track[0])
        track_height = len(env_info.track)
        self.track_cell_size = cell_size
        width = track_width * self.track_cell_size + 2 * self.MARGIN
        height = (
            4 * self.MARGIN  #
            + track_height * self.track_cell_size  # track
            + 2 * self.FONT_SIZE  # status lines
        )
        self.screen = pygame.display.set_mode((width, height),
                                              flags=pygame.RESIZABLE)
        self.font = pygame.font.SysFont('', self.FONT_SIZE)
        self.track_height = track_height * self.track_cell_size
        self._draw_track_first(env_info)

    def _cell_pos(self, r: int, c: int) -> tuple[int, int]:
        y = self.MARGIN + self.track_cell_size * r
        x = self.MARGIN + self.track_cell_size * c
        return y, x

    def draw_track(self) -> None:
        self.screen.blit(self.track_surface, (self.MARGIN, self.MARGIN))

    def _draw_track_first(self, env_info: replay.EnvInfo) -> None:
        width = len(env_info.track[0]) * self.track_cell_size
        height = len(env_info.track) * self.track_cell_size
        self.track_surface = pygame.Surface((width, height))
        for row_ind, row in enumerate(env_info.track):
            for col_ind, cell in enumerate(row):
                pygame.draw.rect(
                    self.track_surface, self.TRACK_COLOURS[cell],
                    pygame.Rect(col_ind * self.track_cell_size,
                                row_ind * self.track_cell_size,
                                self.track_cell_size, self.track_cell_size))
        grid_surface = pygame.Surface((width, height)).convert_alpha()
        grid_surface.fill((0, 0, 0, 0))
        for col_ind in range(len(env_info.track[0]) - 1):
            x = (col_ind+1) * self.track_cell_size
            pygame.draw.line(
                grid_surface,
                self.TRACK_GRID_COLOUR, (x, 0), (x, height),
                width=1)
        for row_ind in range(len(env_info.track) - 1):
            y = (row_ind+1) * self.track_cell_size
            pygame.draw.line(
                grid_surface,
                self.TRACK_GRID_COLOUR, (0, y), (width, y),
                width=1)
        self.track_surface.blit(grid_surface, (0, 0))

    def draw_players(self, state: replay.State):
        for i, p in enumerate(state.players):
            y, x = self._cell_pos(p.x, p.y)
            y += self.track_cell_size / 4
            pygame.draw.ellipse(
                self.screen, self.PLAYER_COLOURS[i],
                pygame.Rect(x, y, self.track_cell_size,
                            self.track_cell_size / 2))
            pygame.draw.ellipse(
                self.screen,
                self.TRACK_GRID_COLOUR,
                pygame.Rect(x, y, self.track_cell_size,
                            self.track_cell_size / 2),
                width=2)

    def draw_forward_arrows(self, state: replay.State):
        buffer = pygame.Surface(
            (self.track_surface.get_width(),
             self.track_surface.get_height())).convert_alpha()
        buffer.fill((0, 0, 0, 0))
        for i, p in enumerate(state.players):
            x = (p.y + 0.5) * self.track_cell_size
            x2 = (p.y + p.vel_y + 0.5) * self.track_cell_size
            y = (p.x + 0.5) * self.track_cell_size
            y2 = (p.x + p.vel_x + 0.5) * self.track_cell_size
            pygame.draw.line(
                buffer,
                self.PLAYER_COLOURS[i] + [160], (x, y), (x2, y2),
                width=3)
        self.screen.blit(buffer, (self.MARGIN, self.MARGIN))

    def draw_backward_arrows(self, last_state: replay.State,
                             state: replay.State) -> None:
        buffer = pygame.Surface(
            (self.track_surface.get_width(),
             self.track_surface.get_height())).convert_alpha()
        buffer.fill((0, 0, 0, 0))
        for i, (p_old,
                p_now) in enumerate(zip(last_state.players, state.players)):
            x = (p_old.y + 0.5) * self.track_cell_size
            x2 = (p_now.y + 0.5) * self.track_cell_size
            y = (p_old.x + 0.5) * self.track_cell_size
            y2 = (p_now.x + 0.5) * self.track_cell_size
            pygame.draw.line(
                buffer, self.PLAYER_COLOURS[i], (x, y), (x2, y2), width=3)
        self.screen.blit(buffer, (self.MARGIN, self.MARGIN))

    def print_info(self, t: int | str, last_step: Optional[replay.PlayerStep]):
        line = self.font.render(f'Step: {t}', True, self.FONT_COLOUR, 'black')
        # print(line, line.get_width(), line.get_height())
        y = 2 * self.MARGIN + self.track_height
        self.screen.blit(line, (self.MARGIN, y))
        if last_step is not None:
            y += self.MARGIN + self.FONT_SIZE
            step_text = ('invalid move' if not last_step.success else
                         f'last move: dx: {last_step.dx} dy: {last_step.dy}')
            line = self.font.render(
                f'Player {last_step.player_ind}: {step_text}', True,
                self.FONT_COLOUR, 'black')
            self.screen.blit(line, (self.MARGIN, y))

    def draw_all(self, t: int | str, state: replay.State,
                 last_state: Optional[replay.State],
                 last_step: Optional[replay.PlayerStep]) -> None:
        self.screen.fill('black')
        self.draw_track()
        self.draw_players(state)
        self.draw_forward_arrows(state)
        if last_state is not None:
            self.draw_backward_arrows(last_state, state)
        self.print_info(t, last_step)

def app(history: replay.Replay, cell_size: int):
    pygame.init()
    screen = Screen(history.env_info, cell_size)
    clock = pygame.time.Clock()
    running = True
    t = 0
    last_step = None
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT and t < len(history.states) - 1:
                    t += 1
                if event.key == pygame.K_LEFT and t > 0:
                    t -= 1
        if t > 0:
            last_step = history.steps[t - 1]
            last_state = history.states[t - 1]
        else:
            last_step = None
            last_state = None

        screen.draw_all(f'{t}/{len(history.states)-1}', history.states[t],
                        last_state, last_step)

        pygame.display.flip()
        clock.tick(60)

    pygame.quit()

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'replay_file',
        type=str,
        help='Path to the replay file (output by the judge program).')
    parser.add_argument(
        '--cell_size',
        type=int,
        default=Screen.DEFAULT_TRACK_CELL_SIZE,
        help='Size (in pixels) of the cells in the visualisation.')
    return parser.parse_args()

def main():
    args = parse_args()
    history = replay.deserialise(args.replay_file)
    app(history, args.cell_size)

if __name__ == "__main__":
    main()
