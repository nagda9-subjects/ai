# Requirements

- python >= 3.10
- numpy
- pillow

For the visualisation:

- pygame

## Python environment quick start

If you know how to install Python packages, you can safely skip this section.

We advise using [Miniconda](https://docs.conda.io/en/latest/miniconda.html) to 
manage your Python environment. In this section, we will create a virtual 
environment to install the needed packages. This means it is separated from any 
system-wide python environment, the packages and versions (even the Python 
version) can differ.

First, create a new environment named "gridrace":

```bash
conda create --name gridrace python=3.10
```

Then you need to activate it:

```bash
conda activate gridrace
```

After this, if you start a Python executable, it will be loaded from the 
"gridrace" environment. You can check this by checking that the output of `which 
python` points to somewhere within your miniconda installation (e.g., 
`~/miniconda3/envs/gridrace/bin/python`). You should also check the Python 
version by `python --version`.

After this, install the necessary packages:

```bash
conda install numpy pillow pygame
```

# Get started

## Write your bot

The bot communicates with the judge program using standard input and output, 
according to the competition specification. Standard error will be ignored, use 
this for debugging purposes.

Standard input, output and error are all captured in a 
`communications.<date>.log` timestamped file in the current working directory. 

An example bot is provided in the `bot` directory.

## Test your bot

The testing environment is separated into $N+1$ programs (with $N$ being the 
number of bots run simultaneously): the bots and the judge. You should start 
each one from different consoles/terminals so that you can see the outputs.

First, start the judge program in the judge directory:

```bash
python judge/run.py <track_file> <num_players> <visibility_radius>
```

- `track_file` is the path to the map file,
- `num_players` is the number of players, and
- `visibility_radius` specifies how far the agent sees.

`--help` shows more help for each python script. While debugging, you may want 
to increase the timeout parameter using the `--timeout <seconds>` parameter to 
some very high value.

After starting the judge, start each player (bot) in a new console:

```bash
python bot/client_bridge.py <bot_exe>
```

- `bot_exe` is the path to your (or any) bot executable. This is either a python 
  script (with `.py` extension) or a file with executable permissions.

Running the bot will produce a `communications.<date>.log` file that contains 
the communications between the judge and the bot (from the bot's point of view, 
i.e., "stdin" denotes lines that were input to the bot's standard input).

## Watch the replay

Invoking the judge with the `--replay_file <path>` argument will save a replay 
file at the given path. This can be visualised using `visualisation.py`, which 
accepts the replay path as its one argument.

The visualisation is controlled by the left-right arrow keys. (I.e., left: 
backwards in time, right is forward in time.)

If the map is too small, try changing the cell size with the `--cell_size` 
argument (default is 20).
