import socket
import network
import time

from typing import Any, Optional, Callable

PlayerInput = Any

class EnvironmentBase:
    """
    Concrete environments should subclass these, implementing ``reset``,
    ``next_player``, ``observation``, ``read_player_input`` and ``step``.

    A note on observations: there is a reserved string: "~~~END~~~" (in its own
    line), that is used to signal the end of the game. Environments must not
    use this in observations.
    """

    def __init__(self, num_players: int):
        self._num_players = num_players

    def reset(self) -> str:
        raise NotImplementedError()

    def next_player(self, current_player: Optional[int]) -> Optional[int]:
        """
        Calculate the index of the next player, return ``None`` if the game is
        over. ``current_player`` is ``None`` at the beginning, otherwise
        contains the current player index.
        """
        raise NotImplementedError()

    def observation(self, current_player: int) -> str:
        """
        Observation to be sent to the current player

        Can be a multiline string, in which case lines should be separarated by
        "\n"s. The final newline will be appended.
        """
        raise NotImplementedError()

    def read_player_input(
            self, read_line: Callable[[], str]) -> Optional[PlayerInput]:
        """
        Read and optionally parse/validate player input.

        Should take minimal time: player reply timeout is based on the timing
        of this function.

        Returns ``None`` on invalid player input.

        ``read_line`` returns one line of player input (without the ending
        newline, but check ``client_bridge.py`` to be sure).
        """
        raise NotImplementedError()

    def invalid_player_input(self, current_player: int) -> None:
        """
        Handle invalid player input.

        Default is to do nothing.

        Note that timeout also counts as invalid input.
        """

    def step(self, current_player: int,
             player_input: PlayerInput) -> None:
        """
        Apply player action.
        """
        raise NotImplementedError()

    def get_scores(self) -> list[int | float]:
        """
        Return the scores of the players. Called after the end of the game.
        """
        raise NotImplementedError()

    @property
    def num_players(self):
        return self._num_players

class EnvironmentRunner:

    def __init__(self,
                 environment: EnvironmentBase,
                 step_timeout: float,
                 client_addresses: Optional[list[str]] = None):
        self.env = environment
        self.step_timeout = step_timeout
        if client_addresses is not None:
            assert self.env.num_players == len(client_addresses), \
                    'Wrong number of clients for this environment.'
        # Wait for players to connect
        server_socket = socket.create_server(('', network.JUDGE_PORT))
        server_socket.listen(self.env.num_players)
        clients = []
        print('Waiting for players to connect...')
        for _ in range(self.env.num_players):
            (clientsocket, address) = server_socket.accept()
            clientsocket.settimeout(self.step_timeout)
            clients.append((clientsocket, address))
            print('Player connected from', address)
        if client_addresses is not None:
            clients = sorted(
                clients, key=lambda x: client_addresses.index(x[1]))
        self.clients = [c for c, _ in clients]
        server_socket.close()

    def run(self) -> list[int | float]:
        print('Started the run.')
        self._send_initial_observations()
        current_player: Optional[int] = None
        while True:
            current_player = self.env.next_player(current_player)
            if current_player is None:
                break
            assert 0 <= current_player < self.env.num_players
            observation = self.env.observation(current_player)
            if not observation or observation[-1] != '\n':
                observation += '\n'
            self._send_observation(current_player, observation)
            try:
                tick = time.perf_counter()
                player_input = self.env.read_player_input(
                    lambda: self._read_from_client(current_player))
                tock = time.perf_counter()
                if tock - tick > self.step_timeout:
                    player_input = None
            except TimeoutError:
                player_input = None
            except network.NetworkError:
                player_input = None
            if player_input is None:
                self.env.invalid_player_input(current_player)
            else:
                self.env.step(current_player, player_input)
        self._signal_the_end()
        return self.env.get_scores()

    def _send_initial_observations(self) -> None:
        initial_obs = self.env.reset()
        if not initial_obs or initial_obs[-1] != '\n':
            initial_obs += '\n'
        print('Sending initial observation to all players.')
        for p in range(self.env.num_players):
            self._send_observation(p, initial_obs)

    def _signal_the_end(self) -> None:
        print('Run ends, sending the end signal to everyone...')
        for p in range(self.env.num_players):
            self._send_observation(p, '~~~END~~~\n')

    def _send_observation(self, current_player: int, observation: str):
        try:
            network.send_data(self.clients[current_player], observation)
        except BrokenPipeError:
            print(f'Failed to send to player {current_player}.')

    def _read_from_client(self, player_ind: int) -> str:
        msg = network.recv_msg(self.clients[player_ind])
        assert msg['type'] == 'data', 'Control messages aren\'t supported yet.'
        return msg['data']

